# Remasterizar una ISO debian-like

Remasteriza todas las ISO que usen casper.

## Historia

Estos scripts fueron escritos durante la remasterización de Trisquel en
Juan Carlos Radio y mejorados para HackMenta.

## Requisitos

* squashfstools
* libisoburn (o el que tenga `xorriso`)
* genisoimage
* rsync
* xorriso

### Instalación en Mint, Debian y Ubuntu

~~~
sudo apt-get install squashfs-tools libisoburn-dev genisoimage rsync xorriso
~~~

## Uso

Preparar la ISO para remasterizar

    remaster trisquel.iso script-de-cambios archivos_extra

_**Nota:** La imagen original de la distribución tiene que estar en la misma carpeta_

Cuando terminamos, remasterizamos:

    makeiso "Juan Carlos Radio"

## HackMenta

    cd hackmenta
    wget http://mint.c3sl.ufpr.br//stable/17.1/linuxmint-17.1-xfce-32bit.iso
    ../remaster linuxmint-17.1-xfce-32bit.iso makementa *.patch
    ../makeiso "Hack Menta"

_**Nota:** Si queres crear una versión 64bits tenes que usar wget  http://mint.c3sl.ufpr.br//stable/17.1/linuxmint-17.1-xfce-64bit.iso_
